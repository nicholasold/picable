module Picable
  #
  # Downloads flickr images and stores them at a given destination
  #
  class Downloader
    def self.call(files, destination)
      Picable::Downloader.new(files, destination).call
    end

    def initialize(files, destination_folder, concurrency = 10)
      @files = files
      @destination = destination_folder
      @concurrency = concurrency

      FileUtils.mkdir_p(destination_folder)
    end

    def call
      @files.each_slice(@concurrency).each do |group|
        threaded_download(group)
      end
    end

    private

    def threaded_download(group, threads = [])
      group.each do |file|
        threads << Thread.new { download file }
      end
      threads.each(&:join)
    end

    def download(file)
      unless file.downloadable?
        script_error('Error downloading file: Invalid download URL')
      end

      begin
        file.download_file(@destination)
      rescue Mechanize::ResponseCodeError
        script_error("Error downloading file: #{$ERROR_INFO}")
      end
    end

    # TODO: Move ths into a module which handles exceptions
    def script_error(info)
      raise ScriptError, info
    end
  end
end
