module Picable
  #
  # Creates a collage using images from flickr which match given input strings
  #
  #  If I had time, would take all (or most?) of the "private" logic and move
  #  it into it's own context. The main reason for this (apart from simplicity
  #  and readability - which I also think are important) is testability. I
  #  always attempt to build things in a way that is testable, but while
  #  solving/debugging these challenges (which were fun!) but left no time
  #  for making the code testable.. Just ran out of time.
  #
  class Collage
    attr_reader :images

    def initialize(image_strings, destination)
      @image_strings = image_strings
      @destination = "#{destination}/#{Time.now.to_i}"

      @width = Picable::COLLAGE_WIDTH
      @height = Picable::COLLAGE_HEIGHT

      @images = []
    end

    def generate
      download_metadata
      download_image_files
      resize_images
      build
    end

    private

    def download_metadata
      @images = @image_strings.inject([]) do |images, search_string|
        image = Picable::FlickrImage.new
        puts "Searching for am image matching string #{search_string} ..."
        image.download_metadata(search_string)
        images.push(image)
      end
    end

    def download_image_files
      #  puts "Downloading images #{@images.map(&:url)}"
      Picable::Downloader.call(@images, @destination)
    end

    def resize_images
      @images.each do |image|
        image.resize_file(Picable::IMAGE_WIDTH, Picable::IMAGE_HEIGHT)
      end
    end

    def build
      generated_collage = MiniMagick::Tool::Montage.new
      # Dir[Dir.pwd + "/tmp/picable/*.jpg"].each { |image| collage << image }
      Dir["#{@destination}/*.jpg"].each { |image| generated_collage << image }
      add_collage_parameters(generated_collage)
      generated_collage.call
      puts "Collage has been written to #{@destination}/collage.jpg"
    end

    def add_collage_parameters(collage)
      collage << '-mode'
      collage << 'Concatenate'
      collage << '-background'
      collage << 'none'
      collage << '-geometry'
      collage << "#{Picable::COLLAGE_WIDTH}x#{Picable::COLLAGE_HEIGHT}+0+0"
      collage << '-tile'
      collage << Picable::COLLAGE_TILE_TYPE
      collage << "#{@destination}/collage.jpg"
      # collage << Dir.pwd + "#{@destination}/collage.jpg"
    end
  end
end
