# Picable (development instructions can be found at the end)

## Description

Picable is a utiliy for downloading pictures from the flickr website and
then creating a collage from them.  You can specify the types of images
you'd like to download on the command-line. It will then find the most
popular images matching each of the given search strings.

If no search strings are given (or if you give less than 10 search strings)
then the utility will randomly choose the missing search strings for you. It
will do this by randomly selecting them from the dictionary.
(/usr/share/dict/words)

Images downloaded from the flickr website, as well as the collage image
itself, will be placed in an automatically generated folder at the following
location: /tmp/picable.  If you'd like the images placed another folder at a
separate location, then please use the destination flag given under 'Usage'
below.

## Installation (an example gem is located in the pkg folder)

To use picable you will first need to install Imagemagick, ruby, rubygems
and the picable gem itself. You can do so by working your way through the
following instructions:

1. To install Imagemagick.
   On a Mac, this can be done using Homebrew as follows
      brew install imagemagick
   If you are using debian/ubuntu you can do
      sudo apt-get install imagemagick.
   For Windows please follow these instructions
      http://www.imagemagick.org/script/binary-releases.php#windows
2. You will then need ruby and rubygems. These can be installed by following
   the instructions given in the following URLS respectively:
     https://www.ruby-lang.org/en/documentation/installation/ (for ruby)
     https://rubygems.org/pages/download#formats              (for rubygems)
3. You can now install the Gem using the following command:

    gem install picable

## Usage

In order to run picable to create a collage use the following command:

  picable collage [SEARCH_STRINGS] [--destination=DESTINATION]

    SEARCH_STRINGS (optional)
      Creates a collage given a set of search strings. Specify as many as you
      like up to 10.  If you do not give any search strings, or give a number
      less than 10, picable will randomly choose images for you.

    --destination=DESTINATION (optional)
      Feel free to specify an alternative location for your files to be saved
      to. By default they will be place in a folder under /tmp/picable/

To get help for any picable command (currently on the collage command)

  picable help

    COMMAND
      'picable help' will describe the commands which can be used with picable.
      At the moment there are only one COMMAND, 'collage' (described above.)

## Usage examples

  Create a collage at the default location with a bunch of popular but random
  images:

    picable collage

  To create a collage with the most popular image with keyword "nicholas" plus
  nine other images (matching random words pulled from the dictionary) and
  storing them in a folder under the current directory:

    picable collage nicholas --destination=.

  To create a collage with a bunch of interesting farm animals and store the
  results in the default location:

    picable collage hen ox cow pig cat dog cock rabbit sheep goat


## Development

In order to build this project you'll need all the Installation dependencies,
but you will also need to obtain a flickr API key and secret from their
website.  At the time of writing these can be found here:

  https://www.flickr.com/services/api/misc.api_keys.html

Once you've obtained these, you'll need to store them in a .env file at the
top level of the directory structure as follows:

  FLICKR_KEY: [my-flckr-key]
  FLICKR_SECRET: [my-flckr-secret]

Alternatively you'll need to set these as environment variables. Or else you
can also pass them in (As environment variables via the commandline.)

If you run into any trouble, then please give me a buzz at
nicholasold@gmail.com

## Debugging

1. This tool uses your dictionary located at /usr/share/dict/words

If for some reason this has been removed or corrupted you can download a
replacement dictionary online.