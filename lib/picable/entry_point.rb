require 'picable'
#
# Entry point into the utility. Uses Thor command-line manager.
#
# Originally had a few options, like overwriting the dictionary, an
# optional cleanup and verbose mode for logging.  However, Thor can
# get a bit hairy if you try to do anything more than the basic
# commands (and I had trouble getting the "advanced" options working
# given the timeframe.)
#
class EntryPoint < Thor
  desc 'collage [search strings]',
       'creates a collage using a given set of search strings'

  method_option :destination, default: Picable::DEFAULT_DESTINATION

  def collage(*args)
    query_strings = get_query_strings(args, Picable::COLLAGE_SIZE)

    collage = Picable::Collage.new(query_strings, options[:destination])

    collage.generate
  end
end

def get_query_strings(input_strings, total_strings_needed)
  if input_strings.count >= total_strings_needed
    input_strings[0..total_strings_needed - 1]
  else
    num_strings_to_get = total_strings_needed - input_strings.count
    input_strings + get_from_dictionary(num_strings_to_get)
  end
end

def get_from_dictionary(count)
  Picable::Dictionary.new.get_random_words(count)
end
