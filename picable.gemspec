# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'picable/version'

Gem::Specification.new do |spec|
  spec.name          = 'picable'
  spec.version       = Picable::VERSION
  spec.authors       = ['Nicholas Old']
  spec.email         = ['nicholasold@gmail.com']

  spec.summary       = %q{Creates a flickr collage}
  spec.description   = %q{Creates a collages w/- images matching query strings}
  spec.license       = 'WTFPL'
  spec.homepage      = "http://www.flickr.com/"

  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise 'RubyGems 2.0 or newer is required to protect against ' \
      'public gem pushes.'
  end

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = 'exe'
  spec.executables   = ['picable']
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler',    '~> 1.13'
  spec.add_development_dependency 'rake',       '~> 10.0'
  spec.add_development_dependency 'rspec',      '~> 3.0'
  spec.add_development_dependency 'pry-byebug', '~> 3.4', '>= 3.4.2'
  spec.add_development_dependency 'rubocop',    '~> 0.47.1'

  spec.add_runtime_dependency 'dotenv', '~> 2.1', '>= 2.1.2'
  spec.add_dependency 'flickraw',   '~> 0.9.9'
  spec.add_dependency 'thor',       '~> 0.19.4'
  spec.add_runtime_dependency 'mechanize', '~> 2.7', '>= 2.7.5'
  spec.add_runtime_dependency 'mini_magick', '~> 4.6', '>= 4.6.0'
end
