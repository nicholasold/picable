module Picable
  #
  # A flickr image object which can (given a search string) pull down its own
  # metadata from flickr, create a local file given that metadata, and resize
  # itself.
  #
  # Most of the "private" logic below should be pulled out into it's a new
  # location to make it more testable.
  #
  # There is plenty of cleaning up that could be done - especially tweaking the
  # resize method to have some more smarts. Right now just chops the image
  # without too much thought into how the final version looks (though, it does
  # use the center of the image.) I found a few examples of some smart cropping
  # algorithms but didn't have time to get them working.
  #
  class FlickrImage
    attr_reader :metadata

    def initialize
      @metadata = {}
      @remote_path = '' # Set when metadata has been extracted
      @local_path = ''  # Set when file has been downloaded
    end

    def download_metadata(search_string, retries = Picable::DEFAULT_DOWNLOAD_RETRIES)
      unless (metadata = reliable_metadata_downloader(search_string, retries))
        raise ScriptError, "unable to find metadata after #{@retries} tries"
      end

      @metadata = metadata
      @remote_path = @metadata.url_s
    end

    def download_file(destination)
      file = Mechanize.new.get(@remote_path)
      filename = File.basename(file.uri.to_s.split('?')[0])
      @local_path = "#{destination}/#{filename}"

      return if File.exist?(@local_path)

      file.save_as(@local_path)
      puts "Downloaded image from #{@remote_path}"
    end

    def resize_file(width = 320, height = 240)
      unless was_downloaded?
        script_error("No file downloaded to resize #{@metadata}")
      end

      MiniMagick::Image.new(@local_path) do |b|
        b.crop "#{width}x#{height}+0+0"
        b.gravity 'Center'
      end
    end

    def was_downloaded?
      @local_path != ''
    end

    def downloadable?
      @remote_path != ''
    end

    private

    def reliable_metadata_downloader(search_string, retries)
      if (metadata = get_metadata_from_flickr(search_string))
        return metadata
      end

      search_string = dictionary.get_random_words[0]
      puts "Retrying with new search string #{search_string}"
      reliable_metadata_downloader(search_string, retries - 1)
    end

    def dictionary
      @dictionary ||= Picable::Dictionary.new
    end

    def get_metadata_from_flickr(search_string)
      params = flickr_params(text: search_string)

      flickr.photos.search(params)[0]
    end

    def flickr_params(params = {})
      params.merge(Picable::DEFAULT_FLICKR_PARAMS)
    end

    # TODO: Move ths into a module which handles exceptions
    # Plus add more exception handling
    def script_error(info)
      raise ScriptError, info
    end
  end
end
