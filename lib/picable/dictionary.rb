module Picable
  #
  # Accesses and reteives random words from the given dictionary
  # No matter how many words are requested, it gets them in 1-pass
  # However, if we could guarantee a smaller dictionary we could
  # store it in memory on initialise (in case of multiple reads.)
  #
  # Therefore, an improvement would be to check the file size on
  # Dictionary obj creation. If it is below a certain size then store
  # it in memory.
  #
  # Finally, the tool does not check that the dictionary itself even
  # exists, or give the user the opportunity to overwrite it. These
  # could both be relatively easily done, but just ran out of time.
  #
  class Dictionary
    def initialize(filename = Picable::DEFAULT_DICTIONARY)
      validate_file_existence(filename)
      @file_size = calcuate_file_size(filename)
      @filename = filename
    end

    def get_random_words(number_to_get = 1)
      word_indexes = calculate_line_indexes(number_to_get, @file_size)
      extract_words_from_dictionary(word_indexes)
    end

    private

    def calculate_line_indexes(number_of_indexes, max_range)
      Array.new(number_of_indexes).map { rand(0..max_range) }.sort
    end

    # 1-pass word extractor. Does not keep dictionary in memory
    def extract_words_from_dictionary(word_indexes)
      File.foreach(@filename).select.with_index do |_line, line_number|
        word_indexes.include? line_number
      end.map(&:chomp)
    end

    def validate_file_existence(filename)
      raise ScriptError, "invalid dictionary file #{filename}" unless File.file?(filename)
    end

    def calcuate_file_size(filename)
      # Could alternatively use ruby to count
      `wc -l #{filename}`.split.first.to_i
    end
  end
end
