require 'dotenv/load'
require 'thor'
require 'flickraw'
require 'mechanize'
require 'mini_magick'
require 'English'
#
# Utility entry point is picable/entry_point.rb
# TODO: Manage loading and accessing submodules and classes a little nicer,
#       should only include logic in the file that uses it.
#       Also, maybe put contants in their own file? (depending on convention.)
#
module Picable
  autoload :Version,      'picable/version'
  autoload :Dictionary,   'picable/dictionary'
  autoload :Collage,      'picable/collage'
  autoload :FlickrImage,  'picable/flickr_image'
  autoload :Downloader,   'picable/downloader'

  FlickRaw.api_key = ENV['FLICKR_KEY']
  FlickRaw.shared_secret = ENV['FLICKR_SECRET']

  IMAGE_WIDTH = 160
  IMAGE_HEIGHT = 120

  COLLAGE_WIDTH = 320
  COLLAGE_HEIGHT = 240
  COLLAGE_TILE_TYPE = 'x2'.freeze

  COLLAGE_SIZE = 10
  DEFAULT_DICTIONARY = '/usr/share/dict/words'.freeze
  DEFAULT_DESTINATION = '/tmp/picable'.freeze

  DEFAULT_DOWNLOAD_RETRIES = 10
  DEFAULT_FLICKR_PARAMS = {
    media: 'photos',
    extras: 'url_sq, url_t, url_s, url_q, url_m, url_n, url_z, url_c, url_l',
    sort: 'interestingness-desc',
    per_page: 1,
    content_type: 1,
    ispublic: 1
  }.freeze
end
